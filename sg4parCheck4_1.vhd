--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

--Stimuli Generator for parCheck4_1
library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;





entity sg4parCheck4_1 is
    port (
        a_sg : out std_logic;
        b_sg : out std_logic;
		c_sg : out std_logic;
        d_sg : out std_logic
    );
end entity sg4parCheck4_1;





architecture behSg of sg4parCheck4_1 is
begin

    sg:
    process is
    begin
        a_sg <= '0'; b_sg <= '0'; c_sg <= '0'; d_sg <= '0'; wait for 50 ns;
        a_sg <= '1'; b_sg <= '0'; c_sg <= '0'; d_sg <= '0'; wait for 50 ns;
        a_sg <= '0'; b_sg <= '1'; c_sg <= '0'; d_sg <= '0'; wait for 50 ns;
        a_sg <= '1'; b_sg <= '1'; c_sg <= '0'; d_sg <= '0'; wait for 50 ns;
        a_sg <= '0'; b_sg <= '0'; c_sg <= '1'; d_sg <= '0'; wait for 50 ns;
        a_sg <= '1'; b_sg <= '0'; c_sg <= '1'; d_sg <= '0'; wait for 50 ns;
        a_sg <= '0'; b_sg <= '1'; c_sg <= '1'; d_sg <= '0'; wait for 50 ns;
        a_sg <= '1'; b_sg <= '1'; c_sg <= '1'; d_sg <= '0'; wait for 50 ns;
        a_sg <= '0'; b_sg <= '0'; c_sg <= '0'; d_sg <= '1'; wait for 50 ns;
        a_sg <= '1'; b_sg <= '0'; c_sg <= '0'; d_sg <= '1'; wait for 50 ns;
        a_sg <= '0'; b_sg <= '1'; c_sg <= '0'; d_sg <= '1'; wait for 50 ns;
        a_sg <= '1'; b_sg <= '1'; c_sg <= '0'; d_sg <= '1'; wait for 50 ns;
        a_sg <= '0'; b_sg <= '0'; c_sg <= '1'; d_sg <= '1'; wait for 50 ns;
        a_sg <= '1'; b_sg <= '0'; c_sg <= '1'; d_sg <= '1'; wait for 50 ns;
        a_sg <= '0'; b_sg <= '1'; c_sg <= '1'; d_sg <= '1'; wait for 50 ns;
        a_sg <= '1'; b_sg <= '1'; c_sg <= '1'; d_sg <= '1'; wait for 50 ns;
        wait;
    end process sg;
    
end architecture behSg;
