--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

-- Testbench for parCheck4_1
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

library work;
use work.all;

entity tbParCheck4_1 is
end tbParCheck4_1;

architecture behTb of tbParCheck4_1 is
    signal a_s : std_logic;
    signal b_s : std_logic;
    signal c_s : std_logic;
    signal d_s : std_logic;
    signal o_s : std_logic;

    component sg4parCheck4_1 is
        port(
            a_sg : out std_logic;
			b_sg : out std_logic;
			c_sg : out std_logic;
			d_sg : out std_logic
        );
    end component sg4parCheck4_1;
    for all : sg4parCheck4_1 use entity work.sg4parCheck4_1( behSg );

    component parCheck4_1
	    port(	
		    a : in std_logic;
            b : in std_logic;
            c : in std_logic;
            d : in std_logic;
			o : out std_logic
	    );
    end component;
    for all: parCheck4_1 use entity work.parCheck4_1 ( behDut );

begin
	sg_i : sg4parCheck4_1
        port map (
            a_sg => a_s,
            b_sg => b_s,
			c_sg => c_s,
			d_sg => d_s
        );
    
    parCheck4_1_i : parCheck4_1
        port map (
            o => o_s,
            a => a_s,
            b => b_s,
			c => c_s,
			d => d_s
        );
end behTb;