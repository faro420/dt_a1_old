--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

--VHDL code for even parity check
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity parCheck4_1 is
    port(
        o : out std_logic;
        a : in std_logic;
        b : in std_logic;
        c : in std_logic;
        d : in std_logic
    );
end entity parCheck4_1;

architecture behDut of parCheck4_1 is
begin
    process(a, b, c, d) is
        variable a_v : std_logic;
        variable b_v : std_logic;
        variable c_v : std_logic;
        variable d_v : std_logic;
        variable res_v: std_logic;
    begin
        a_v := a;
        b_v := b;
        c_v := c;
        d_v := d;
        
        res_v := NOT (a_v XOR (b_v XOR(c_v XOR d_v)));
        
        o <= res_v;
    end process;
end architecture behDut;
